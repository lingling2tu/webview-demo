# ! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2023/10/20 08:43
# software: PyCharm

from PyQt6.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout


class CombWidget:

    @staticmethod
    def combHBox(lst: list):
        h = QHBoxLayout()
        for x, y in lst:
            h.addWidget(x, y)
        w = QWidget()
        w.setLayout(h)
        return w

    @staticmethod
    def combVBox(lst: list):
        h = QVBoxLayout()
        for x, y in lst:
            h.addWidget(x, y)
        w = QWidget()
        w.setLayout(h)
        return w
