#! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2020/5/12 0:02
# software: PyCharm
import time, sys
from lib.Log import Log

def Singletonfunc(cls):
    _instance = {}
    def _singleton(*args, **kargs):
        if cls not in _instance:
            _instance[cls] = cls(*args, **kargs)
        return _instance[cls]

    return _singleton


@Singletonfunc
class Error:
    def trigger(self, msg):
        now = time.strftime("%Y-%m-%d %H:%M:%S")
        # __file__ 当前文件
        # sys._getframe().f_lineno 当前行
        content = "[error] {0} file:{1} line:{2}:{3}\n".format(now, __file__, sys._getframe().f_lineno, msg)
        print(content)
        Log().error(str(msg), "error")

if __name__ == "__main__":
    Error().trigger("test 111")