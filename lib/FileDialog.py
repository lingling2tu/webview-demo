#! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2020/8/31 15:18
# software: PyCharm

from PyQt6.QtWidgets import QAbstractItemView, QListView, QTreeView, QFileDialog


class FileDialog(QFileDialog):
    def __init__(self, *args):
        QFileDialog.__init__(self, *args)
        self.setWindowTitle("选择目录")
        self.setOption(self.DontUseNativeDialog, True)
        self.setFileMode(self.DirectoryOnly)

        self.tree = self.findChild(QTreeView)
        self.tree.setSelectionMode(QAbstractItemView.MultiSelection)

        self.list = self.findChild(QListView)
        self.list.setSelectionMode(QAbstractItemView.MultiSelection)

    #     self.filesSelected.connect(self.showfiles)
    #
    # def showfiles(self,s):
    #     print(s)
