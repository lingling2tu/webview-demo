#! /usr/bin/python
# -*- coding: utf-8 -*-
# author:jatvsjat
# datetime:2023/10/20 08:40
# software: PyCharm

from PyQt6.QtWidgets import QWidget, QMainWindow
from PyQt6.QtCore import QObject, pyqtSignal


def Singletonfunc(cls):
    _instance = {}

    def _singleton(*args, **kargs):
        if cls not in _instance:
            _instance[cls] = cls(*args, **kargs)
        return _instance[cls]

    return _singleton


@Singletonfunc
class Signal(QObject):
    _mainClose = pyqtSignal(int)
    triggerStream = pyqtSignal(bytes)


# 关联窗口 - 主窗口
class RMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self._signal = Signal()

    def closeEvent(self, e):
        print("close.. main window")
        self._signal._mainClose.emit(1)
        super().closeEvent(e)


# 关联窗口 - 主窗口
class RMainWidget(QWidget):
    def __init__(self):
        super().__init__()
        self._signal = Signal()

    def closeEvent(self, e):
        print("close.. main widget")
        self._signal._mainClose.emit(1)
        super().closeEvent(e)


# 关联窗口 widget - 子窗口
class RSubWidget(QWidget):
    def __init__(self, win=None):
        super().__init__(win)
        _signal = Signal()
        _signal._mainClose[int].connect(self._sub_close)

    def _sub_close(self, flag):
        if flag:
            print("sub close")
            self.close()


# 关联窗口 mainwindow - 子窗口
class RSubMainWindow(QMainWindow):
    def __init__(self, win=None):
        super().__init__(win)
        _signal = Signal()
        _signal._mainClose[int].connect(self._sub_close)

    def _sub_close(self, flag):
        if flag:
            print("sub close")
            self.close()
