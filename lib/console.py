#! /usr/bin/python
# -*- coding: utf-8 -*-
# author:jatvsjat
# datetime:2019/8/25 8:04
# software: PyCharm

import sys

from PyQt6.QtWidgets import QVBoxLayout, QTextEdit, QApplication, QPushButton
from PyQt6.QtCore import QObject, pyqtSignal, Qt
from PyQt6.QtGui import QGuiApplication, QTextCursor, QIcon
from lib.RelateWin import RSubWidget


class EmittingStream(QObject):
    textWritten = pyqtSignal(str)  # 定义一个发送str的信号

    def write(self, text):
        self.textWritten.emit(str(text))
        # 首页类


class Console(RSubWidget):
    def __init__(self):
        super().__init__()
        self.btn = None
        self.textEdit = None
        self.setupUi()

    def setupUi(self):
        self.setWindowFlags(Qt.WindowType.WindowStaysOnTopHint)
        self.setWindowIcon(QIcon("./static/console.png"))

        self.textEdit = QTextEdit("")
        self.textEdit.setReadOnly(True)

        # 下面将输出重定向到textEdit中
        sys.stdout = EmittingStream(textWritten=self.outputWritten)
        sys.stderr = EmittingStream(textWritten=self.outputWritten)

        print("hello console")

        # 测试效果
        self.btn = QPushButton("清理控制台", self)
        self.btn.clicked.connect(self.clear)

        v = QVBoxLayout()
        v.addWidget(self.btn)
        v.addWidget(self.textEdit)
        self.setLayout(v)

        self.setGeometry(100, 100, 500, 600)
        self.center()
        self.setWindowTitle("控制台")

    def clear(self):
        # print("清空控制台...")
        self.textEdit.setText("")

    def checkClear(self):
        if len(self.textEdit.toPlainText().split("\n")) > 1000:
            self.clear()

    def center(self):
        qr = self.frameGeometry()
        cp = QGuiApplication.primaryScreen().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    # 接收信号str的信号槽
    def outputWritten(self, text):
        cursor = self.textEdit.textCursor()
        cursor.movePosition(cursor.MoveOperation.End)
        # cursor.insertText("{0}:{1}\n".format(time.strftime("%H:%M:%S"), text))
        cursor.insertText(text)
        self.textEdit.setTextCursor(cursor)
        self.textEdit.ensureCursorVisible()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    cls_app = Console()
    cls_app.show()
    sys.exit(app.exec())
