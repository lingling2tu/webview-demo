#! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2019/10/27 21:00
# software: PyCharm
from PyQt6.QtCore import QObject, pyqtSignal


def Singletonfunc(cls):
    _instance = {}

    def _singleton(*args, **kwargs):
        if cls not in _instance:
            _instance[cls] = cls(*args, **kwargs)
        return _instance[cls]

    return _singleton


@Singletonfunc
class Communicate(QObject):
    title = pyqtSignal(str)
    triggerCookieFile = pyqtSignal(str)
    triggerCookieStr = pyqtSignal(str)
    triggerClearCookieStr = pyqtSignal(bool)
