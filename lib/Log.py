#! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2020/5/11 23:48
# software: PyCharm
import logging.handlers
import time
app_debug = True

def Singletonfunc(cls):
    _instance = {}

    def _singleton(*args, **kargs):
        if cls not in _instance:
            _instance[cls] = cls(*args, **kargs)
        return _instance[cls]

    return _singleton


@Singletonfunc
class Logger:
    def __init__(self):
        self.file = "./data/test.log"
        logger = logging.getLogger()
        #hdlr = logging.handlers.TimedRotatingFileHandler(filename=self.file, when='M', interval=5, backupCount=100,encoding="utf-8")

        formatter = logging.Formatter("%(asctime)s %(filename)s [line:%(lineno)d] %(levelname)s %(message)s",
                                     datefmt="%Y-%m-%d %H:%M:%S")
        hdlr = logging.FileHandler(filename=self.file, mode="a+", encoding='utf-8')
        hdlr.setFormatter(formatter)
        logger.addHandler(hdlr)
        logger.setLevel(logging.INFO)
        self.logging = logger


    def instance(self):
        return self.logging

    def info(self, msg):
        return;

    def warning(self, msg):
        return;

    def debug(self, msg):
        return;

    def error(self, msg):
        return;

    def critical(self, msg):
        return;

def date_record(msg):
        filename = time.strftime("%Y-%m-%d", time.localtime())
        with open("./data/{}.log".format(filename),"a",encoding="utf-8") as f:
            f.write(msg)


def Log():
    if(app_debug):
        return Logger().instance()
    else:
        return Logger();

if __name__ == "__main__":
    Log().info("test 111")
