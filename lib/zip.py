#! /usr/bin/python
#-*- coding: utf-8 -*-
# author:凌
# datetime:2020/8/31 17:15
# software: PyCharm


#! /usr/bin/python
#-*- coding: utf-8 -*-
# author:jatvsjat
# datetime:2019/4/10 17:07
# software: PyCharm


import zipfile
import os

def zipper(startdir,file_news):
    #startdir = ".\\123"  #要压缩的文件夹路径
    #file_news = startdir +'.zip' # 压缩后文件夹的名字
    z = zipfile.ZipFile(file_news,'w',zipfile.ZIP_DEFLATED) #参数一：文件夹名
    for dirpath, dirnames, filenames in os.walk(startdir):
        fpath = dirpath.replace(startdir,'') #这一句很重要，不replace的话，就从根目录开始复制
        fpath = fpath and fpath + os.sep or ''#这句话理解我也点郁闷，实现当前文件夹以及包含的所有文件的压缩
        for filename in filenames:
            z.write(os.path.join(dirpath, filename),fpath+filename)
            print ('%s zipped ok.'% (fpath+filename))
    z.close()
    print("压缩完成 %s" % (file_news,))

def unzip(filename,path):
    r = zipfile.is_zipfile(filename)
    if r:
        file_zip = zipfile.ZipFile(filename, 'r')
        for file in file_zip.namelist():
            file_zip.extract(file, path)
            print("unzip %s" % (path+file))
        file_zip.close()
        print("解压完成")
    else:
        print("这不是一个压缩文件")


def zipinfo(file_zip,filename):
    zipFile = zipfile.ZipFile(file_zip, "r")
    zipInfo = zipFile.getinfo(filename)
    print('filename:', zipInfo.filename)
    print('date_time:', zipInfo.date_time)
    print('compress_type:', zipInfo.compress_type)
    print('comment:', zipInfo.comment)
    print('extra:', zipInfo.extra)
    print('create_system:', zipInfo.create_system)
    print('create_version:', zipInfo.create_version)
    print('extract_version:', zipInfo.extract_version)
    print('extract_version:', zipInfo.reserved)
    print('flag_bits:', zipInfo.flag_bits)
    print('volume:', zipInfo.volume)
    print('internal_attr:', zipInfo.internal_attr)
    print('external_attr:', zipInfo.external_attr)
    print('header_offset:', zipInfo.header_offset)
    print('CRC:', zipInfo.CRC)
    print('compress_size:', zipInfo.compress_size)
    print('file_size:', zipInfo.file_size)
    zipFile.close()

