#! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2020/3/1 16:00
# software: PyCharm


from configparser import ConfigParser


class Conf:

    def __init__(self, file):
        self.cf = None
        self.file = file

    def getInstance(self):
        self.cf = ConfigParser()
        self.cf.read(self.file, encoding="utf-8")
        return self.cf

    def save(self):
        with open(self.file, "w", encoding="utf-8") as f:
            self.cf.write(f)

    @staticmethod
    def create():
        cf = ConfigParser()
        conf_file = "./conf.ini"
        cf.read(conf_file)
        cf.add_section("settings")
        cf.set("settings", "url", "test")
        with open(conf_file, "w", encoding="utf-8") as f:
            cf.write(f)
