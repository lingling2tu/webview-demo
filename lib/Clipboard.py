#! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2019/10/2 23:32
# software: PyCharm
import win32con
import win32clipboard as wincld


class Clipboard:

    def getText(self):
        wincld.OpenClipboard()
        text_result = wincld.GetClipboardData(win32con.CF_UNICODETEXT)
        wincld.CloseClipboard()
        return text_result

    def setText(self, info):
        wincld.OpenClipboard()
        wincld.EmptyClipboard()
        wincld.SetClipboardData(win32con.CF_UNICODETEXT, info)
        wincld.CloseClipboard()
