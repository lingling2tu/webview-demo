# ! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2023/10/19 16:54
# software:PyCharm
import sys

from PyQt6.QtCore import Qt, QUrl
from PyQt6.QtWebEngineWidgets import QWebEngineView
from PyQt6.QtWidgets import QApplication, QMainWindow


class WebEngineView(QWebEngineView):
    def __init__(self, parent=None):
        super().__init__(parent)

    def contextMenuEvent(self, event):
        # 禁止默认的右键菜单
        return None


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        url = "http://admins.com/test.html"
        # 创建一个 WebEngineView
        self.view = WebEngineView(self)
        # 禁用右键菜单
        self.view.contextMenuEventPolicy = Qt.ContextMenuPolicy.NoContextMenu
        # 安装事件过滤器，防止 child 接收鼠标键盘事件信息
        self.view.installEventFilter(self)

        self.view.load(QUrl(url))

        self.setCentralWidget(self.view)
        self.view.resize(1000, 552)
        self.setGeometry(100, 100, 1000, 552)


app = QApplication(sys.argv)
window = MainWindow()
window.show()
sys.exit(app.exec())
