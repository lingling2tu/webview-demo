# ! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2023/10/20 22:07
# software:PyCharm
# 导入所需的模块
from PyQt6.QtWidgets import QApplication, QMainWindow, QTextEdit, QPushButton
from PyQt6.QtCore import QObject, pyqtSignal
import sys


# 定义一个重定向信号类，用于将 print 输出发送到 textWritten 信号
class EmittingStream(QObject):
    textWritten = pyqtSignal(str)

    def write(self, text):
        self.textWritten.emit(str(text))


# 定义一个主窗口类，继承自 QMainWindow
class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        # 设置窗口标题和大小
        self.setWindowTitle("PyQt6 控制台输出示例")
        self.resize(600, 400)
        # 创建一个文本编辑框，用于显示控制台输出
        self.textEdit = QTextEdit(self)
        self.textEdit.setReadOnly(True)
        self.setCentralWidget(self.textEdit)
        # 创建一个按钮，用于触发打印操作
        self.button = QPushButton("打印", self)
        self.button.move(250, 350)
        self.button.clicked.connect(self.print_something)
        # 将标准输出重定向到 EmittingStream 实例
        sys.stdout = EmittingStream()
        # 将 textWritten 信号连接到 update_text 方法
        sys.stdout.textWritten.connect(self.update_text)

    # 定义一个方法，用于更新文本编辑框的内容
    def update_text(self, text):
        # 获取文本编辑框的光标
        cursor = self.textEdit.textCursor()
        # 将光标移动到末尾
        cursor.movePosition(cursor.MoveOperation.End)
        # 插入文本
        cursor.insertText(text)
        # 设置文本编辑框的光标
        self.textEdit.setTextCursor(cursor)
        # 确保光标可见
        self.textEdit.ensureCursorVisible()

    # 定义一个方法，用于打印一些内容到控制台
    def print_something(self):
        print("Hello, this is PyQt6!")
        print("This is a demo of displaying console output in a text edit widget.")
        print("You can use the EmittingStream class to redirect the standard output to a signal.")


# 创建一个应用实例
app = QApplication(sys.argv)
# 创建一个主窗口实例
window = MainWindow()
# 显示主窗口
window.show()
# 运行应用循环
sys.exit(app.exec())
