# ! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2023/10/17 15:54
# software:PyCharm

import sys
from PyQt6.QtWidgets import QApplication
from PyQt6.QtGui import QGuiApplication

from lib.RelateWin import RMainWidget
from lib.console import Console


class App(RMainWidget):

    def __init__(self):
        super().__init__()
        self.console = None
        self.initUI()

    def initUI(self):
        self.setWindowTitle("PyQt6 窗口案例")
        self.console = Console()
        self.console.show()

        self.setGeometry(100, 100, 400, 300)
        self.center()

    def center(self):
        qr = self.frameGeometry()
        cp = QGuiApplication.primaryScreen().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    cls_app = App()
    cls_app.show()
    sys.exit(app.exec())
