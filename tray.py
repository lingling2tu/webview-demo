# ! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2023/10/17 15:54
# software:PyCharm

import sys
from PyQt6.QtWidgets import QApplication, QMainWindow, QSystemTrayIcon, QMenu
from PyQt6.QtGui import QGuiApplication, QIcon, QAction


class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle("PyQt6 窗口系统托盘")

        self.setTray()

        self.setGeometry(100, 100, 400, 300)
        self.center()

    def setTray(self):
        # 创建一个系统托盘图标对象
        self.tray_icon = QSystemTrayIcon(self)

        # 设置图标为一个png文件
        self.tray_icon.setIcon(QIcon("logo_16.png"))

        # 设置图标的提示信息
        self.tray_icon.setToolTip("这是一个窗口系统托盘")

        # 创建一个菜单对象，用于显示在系统托盘的右键菜单中
        self.tray_menu = QMenu()

        # 创建一些菜单项，用于执行不同的操作
        self.show_action = QAction("显示窗口", triggered=self.showWindow)
        self.hide_action = QAction("隐藏窗口", triggered=self.hideWindow)
        self.quit_action = QAction("退出程序", triggered=QApplication.instance().quit)

        # 将菜单项添加到菜单中
        self.tray_menu.addAction(self.show_action)
        self.tray_menu.addAction(self.hide_action)
        self.tray_menu.addSeparator()
        self.tray_menu.addAction(self.quit_action)

        # 将菜单设置为系统托盘图标的右键菜单
        self.tray_icon.setContextMenu(self.tray_menu)
        print("tray show")
        # 显示系统托盘图标
        self.tray_icon.show()

    def showWindow(self):
        # 显示窗口，并将其激活为当前窗口
        self.show()
        self.activateWindow()

    def hideWindow(self):
        # 隐藏窗口
        self.hide()

    def center(self):
        qr = self.frameGeometry()
        cp = QGuiApplication.primaryScreen().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    cls_app = App()
    cls_app.show()
    sys.exit(app.exec())
