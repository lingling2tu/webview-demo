#! /usr/bin/python
# -*- coding: utf-8 -*-
# author:jatvsjat
# datetime:2019/8/26 17:30
# software: PyCharm


import os, shutil
import re


def convert_path(path):
    seps = r'\/'
    sep_other = seps.replace(os.sep, '')
    return path.replace(sep_other, os.sep)


def list_dir(path):
    dirs = []
    for path, name, filenames in os.walk(path):
        print(path)
        dirs.append(path)

    return dirs


def copyapp(conf_files, path, distpath):
    if len(conf_files) > 0:
        for file in conf_files:
            f = os.path.join(path, file)
            d = os.path.join(distpath, file)
            if len(file.split(":\\")) > 1:
                d = os.path.join(distpath, os.path.basename(file))

            if os.path.exists(d) == False:
                if os.path.isdir(f):
                    shutil.copytree(f, d)
                else:
                    shutil.copyfile(f, d)

                print("copy: %s -> %s" % (f, d))
            print("#====== 导入配置文件 ======")


def main():
    path = os.getcwd()
    print(path)
    distpath = os.path.join(path, "dist")

    # 导入 设定的内容
    conf_files = [
        "./images",
        "./conf.ini",
        "./cache",

    ]

    for dir in os.listdir(distpath):
        dtpath = os.path.join(distpath, dir)
        if os.path.isdir(dtpath):
            # 处理配置文件
            copyapp(conf_files, path, dtpath)


if __name__ == "__main__":
    main()
