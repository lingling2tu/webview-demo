# ! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2023/10/19 09:48
# software:PyCharm
# pip install pyqt6-webengine
# 导入必要的模块
from PyQt6.QtWebEngineCore import QWebEngineProfile
from PyQt6.QtWidgets import QApplication, QMainWindow, QLabel, QLineEdit, QPushButton, QMessageBox
from PyQt6.QtCore import QUrl
from PyQt6.QtWebEngineWidgets import QWebEngineView


# 创建一个主窗口类，继承自 QMainWindow
class MainWindow(QMainWindow):
    # 重写 __init__ 方法
    def __init__(self):
        # 调用父类的 __init__ 方法
        super().__init__()
        # 设置窗口标题和大小
        self.setWindowTitle("PyQt6 QWebEngineView Example")
        self.resize(800, 600)
        # 调用 setupUi 方法
        self.setupUi()
        # 调用 retranslateUi 方法
        self.retranslateUi()

    # 定义 setupUi 方法
    def setupUi(self):
        # 创建一个 QWebEngineView 对象，并设置为窗口的中心控件
        self.webview = QWebEngineView(self)
        self.setCentralWidget(self.webview)
        # 获取网页的配置对象，并设置缓存路径和缓存类型
        profile = self.webview.page().profile()
        profile.setPersistentStoragePath("./cache")  # 设置缓存路径为当前目录下的 cache 文件夹
        profile.setHttpCacheType(QWebEngineProfile.HttpCacheType.DiskHttpCache)  # 设置缓存类型为磁盘缓存
        # 创建一个 QLabel 对象，用于显示 "输入网址：" 的文本，并设置为窗口的状态栏左侧控件
        self.label = QLabel(self)
        self.statusBar().addPermanentWidget(self.label)
        # 创建一个 QLineEdit 对象，用于输入网址，并设置为窗口的状态栏中间控件
        self.url_edit = QLineEdit(self)
        self.statusBar().addWidget(self.url_edit)
        # 创建一个 QPushButton 对象，用于触发加载网页的操作，并设置为窗口的状态栏右侧控件
        self.go_button = QPushButton(self)
        self.statusBar().addPermanentWidget(self.go_button)
        # 将 QLineEdit 的 returnPressed 信号连接到 loadUrl 槽函数
        self.url_edit.returnPressed.connect(self.loadUrl)
        # 将 QPushButton 的 clicked 信号连接到 loadUrl 槽函数
        self.go_button.clicked.connect(self.loadUrl)
        # 将 QWebEngineView 的 urlChanged 信号连接到 updateUrl 槽函数
        self.webview.urlChanged.connect(self.updateUrl)

    # 定义 retranslateUi 方法
    def retranslateUi(self):
        # 设置 QLabel 的文本为 "输入网址："
        self.label.setText("输入网址：")
        # 设置 QPushButton 的文本为 "→"
        self.go_button.setText("→")

    # 定义 loadUrl 槽函数
    def loadUrl(self):
        # 获取 QLineEdit 中输入的网址字符串，并转换为 QUrl 对象
        url = QUrl(self.url_edit.text())
        # 如果 QUrl 对象是有效的，则使用 QWebEngineView 加载该网址；否则弹出警告框提示用户输入有效的网址
        if url.isValid():
            self.webview.load(url)
        else:
            QMessageBox.warning(self, "Warning", "请输入有效的网址")

    # 定义 updateUrl 槽函数
    def updateUrl(self, url):
        # 将 QUrl 对象转换为字符串，并设置到 QLineEdit 中
        self.url_edit.setText(url.toString())


# 创建一个 QApplication 对象
app = QApplication([])
# 创建一个 MainWindow 对象
window = MainWindow()
# 显示窗口
window.show()
# 进入应用程序的主循环
app.exec()
