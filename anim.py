# ! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2023/10/17 15:54
# software:PyCharm

import sys
from PyQt6.QtWidgets import QApplication, QMainWindow, QWidget
from PyQt6.QtGui import QGuiApplication, QIcon
from PyQt6.QtCore import QPropertyAnimation, Qt, QEasingCurve, QTimer
import time


class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.state = 0
        self.initUI()

    def initUI(self):
        # 设置窗口标题和大小
        self.setWindowTitle("窗口渐隐动画")
        self.setWindowIcon(QIcon('logo.png'))

        self.resize(300, 200)
        self.anim = QPropertyAnimation(self, b"windowOpacity")

        self.timerThread = QTimer()
        self.timerThread.setInterval(2000)
        self.timerThread.timeout.connect(self.winAction)
        self.timerThread.start()

        self.setGeometry(100, 100, 400, 300)
        self.center()

    def winAction(self):
        print(self.state)
        if self.state:
            self.animIn()
        else:
            self.animOut()

    def animOut(self):
        print("win out")
        self.state = 1

        # 设置动画的持续时间为2000毫秒
        self.anim.setDuration(500)
        # 设置动画的起始值为1.0，即完全不透明
        self.anim.setStartValue(1.0)
        # 设置动画的结束值为0.0，即完全透明
        self.anim.setEndValue(0.0)
        # 设置动画的曲线类型为InOutQuad，即先加速后减速
        self.anim.setEasingCurve(QEasingCurve.Type.InOutQuad)
        self.anim.start()

    def animIn(self):
        print("win in")
        self.state = 0

        # 设置动画的持续时间为2000毫秒
        self.anim.setDuration(500)
        # 设置动画的起始值为1.0，即完全不透明
        self.anim.setStartValue(0.0)
        # 设置动画的结束值为0.0，即完全透明
        self.anim.setEndValue(1.0)
        # 设置动画的曲线类型为InOutQuad，即先加速后减速
        self.anim.setEasingCurve(QEasingCurve.Type.InOutQuad)
        self.anim.start()

    def mousePressEvent(self, event):
        # 当鼠标点击窗口时，启动动画
        self.anim.start()

    def center(self):
        qr = self.frameGeometry()
        cp = QGuiApplication.primaryScreen().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    cls_app = App()
    cls_app.show()
    sys.exit(app.exec())
