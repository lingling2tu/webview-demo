# ! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2023/10/20 08:24
# software:PyCharm
from configparser import ConfigParser

cf = ConfigParser()
conf_file = "./conf.ini"
cf.read(conf_file)
cf.add_section("settings")
cf.set("settings", "url", "manager")
with open(conf_file, "w", encoding="utf-8") as f:
    cf.write(f)
