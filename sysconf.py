# ! /usr/bin/python
# -*- coding: utf-8 -*-
# author:凌
# datetime:2023/10/17 15:54
# software:PyCharm

import sys
from PyQt6.QtWidgets import QApplication, QLineEdit, QPushButton, QLabel, QVBoxLayout, QWidget, QCheckBox
from PyQt6.QtGui import QGuiApplication, QIcon
from PyQt6.QtCore import Qt

from lib.Conf import Conf
from lib.CombWidget import CombWidget
from lib.RelateWin import RMainWindow


class Sysconf(RMainWindow):

    def __init__(self):
        super().__init__()
        self.save_btn = None
        self.url_text = None
        self.conf = Conf("conf.ini")
        self.cf = self.conf.getInstance()

        self.initUI()

    def initUI(self):
        self.setWindowTitle("系统配置")
        self.setWindowModality(Qt.WindowModality.ApplicationModal)
        self.setWindowState(Qt.WindowState.WindowNoState)

        self.statusBar().showMessage("载入配置")

        self.url_text = QLineEdit(self.cf.get("settings", "url"))
        self.url_text.textChanged.connect(self.urlChange)
        self.save_btn = QPushButton("保存")
        self.save_btn.setDisabled(True)
        self.save_btn.clicked.connect(lambda: self.saveConf(self.url_text, 'url'))

        self.tray_check = QCheckBox("系统托盘")
        # 初始化
        if bool(int(self.cf.get("settings", "tray"))):
            self.tray_check.setChecked(True)
        self.tray_check.stateChanged.connect(self.trayCheck)

        w1 = CombWidget.combHBox([(QLabel("默认URL:"), 1), (self.url_text, 10), (self.save_btn, 1)])
        w2 = CombWidget.combHBox([(QLabel("托盘设置"), 1), (self.tray_check, 11)])

        v = QVBoxLayout()
        v.addWidget(w1, 6)
        v.addWidget(w2, 6)
        w = QWidget()
        w.setLayout(v)
        self.setCentralWidget(w)

        self.setWindowFlags(Qt.WindowType.WindowMinimizeButtonHint | Qt.WindowType.WindowCloseButtonHint)
        self.setWindowIcon(QIcon("./images/conf.png"))
        self.setGeometry(100, 100, 500, 150)
        self.setFixedSize(500, 150)

        self.center()

    def urlChange(self, text):
        self.save_btn.setDisabled(False)

    def saveConf(self, input, name):
        self.cf.set("settings", name, input.text())
        self.conf.save()
        self.save_btn.setText("保存成功")
        self.save_btn.setDisabled(True)
        self.statusBar().showMessage("重启程序后生效！")

    def trayCheck(self):
        print("init")
        if self.tray_check.isChecked():
            print("True")
            self.cf.set("settings", "tray", "1")
        else:
            print("False")
            self.cf.set("settings", "tray", "0")
        self.statusBar().showMessage("重启程序后生效！")

    def center(self):
        qr = self.frameGeometry()
        cp = QGuiApplication.primaryScreen().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    cls_app = Sysconf()
    cls_app.show()
    sys.exit(app.exec())
